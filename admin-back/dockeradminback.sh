#!/bin/bash

nombre=adminback
puerto=8081

if docker network ls | grep redfeedback > /dev/null; then
    echo "Ya existe la network redfeedback"
else
    echo "No existe la network redfeedback. Creando..."
    docker network create redfeedback
fi
echo    # 
if docker ps | grep $nombre > /dev/null; then
    echo "La instancia $nombre está activa."
    read -p " ¿Detener? (s/n)" -n 1 -r
    echo    # 
    if [[ $REPLY =~ ^[Ss]$ ]]; then
    	docker stop $nombre
    else
    	exit 0
    fi
fi

if docker ps -a | grep $nombre > /dev/null; then
    echo "La instancia $nombre está parada."
    
    read -p " ¿Eliminar? (s/n)" -n 1 -r    
    echo    # 
    if [[ $REPLY =~ ^[Ss]$ ]]; then

        docker rm -f $nombre    	
    else
	read -p " ¿Arrancar? (s/n)" -n 1 -r
	echo    # 
	if [[ $REPLY =~ ^[Ss]$ ]]; then
            docker start $nombre
	fi   
    fi
fi

if docker ps -a | grep $nombre > /dev/null; then
    echo "mantenemos la imagen existente de $nombre"
else
    echo "Construyendo la imagen $nombre y arrancando."
    echo "============================================================================"
    docker build -t $nombre .
    docker run --net=redfeedback -d -p $puerto:$puerto --name $nombre $nombre
    echo "============================================================================"
fi

echo    # 
echo    #         


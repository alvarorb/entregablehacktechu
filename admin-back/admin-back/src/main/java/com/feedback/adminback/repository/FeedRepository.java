package com.feedback.adminback.repository;

import com.feedback.adminback.model.FeedModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedRepository extends MongoRepository<FeedModel, String> {

    @Query(value = "{ 'idPoll' : ?0}")
    java.util.List < FeedModel > findByIdPoll(String idPoll);
}

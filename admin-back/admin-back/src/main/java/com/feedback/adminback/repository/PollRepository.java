package com.feedback.adminback.repository;

import com.feedback.adminback.model.PollModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PollRepository extends MongoRepository<PollModel, String> {

}

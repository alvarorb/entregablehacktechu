package com.feedback.adminback.service;

import com.feedback.adminback.repository.FeedRepository;
import com.feedback.adminback.model.FeedModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class FeedService {

    @Autowired
    FeedRepository feedRepository;

    // Read
    public List<FeedModel> findAll() {
        return feedRepository.findAll();
    }

    // Obtener TODOS los feeds de un poll
    public List < FeedModel > findByIdPoll(String idPoll){
        return feedRepository.findByIdPoll(idPoll);
    };



    public Optional<FeedModel> findById(String idPoll){
        return feedRepository.findById(idPoll);

    }

    // Create
    public FeedModel save(FeedModel feed){
        return feedRepository.save(feed);
    }

    // Delete
    public boolean delete(FeedModel feed){
        try{
            feedRepository.delete(feed);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    //Confirmar si existe
    public boolean existsByID(String idPoll) {
        return(feedRepository.existsById(idPoll));
    }

    public boolean deleteById(String idPoll){
        if(feedRepository.existsById(idPoll)) {
            feedRepository.deleteById(idPoll);
            return true;
        }
        else {
            return false;
        }
    }

}

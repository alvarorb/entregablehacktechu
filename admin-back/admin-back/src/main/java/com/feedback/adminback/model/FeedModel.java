package com.feedback.adminback.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "feeds")

public class FeedModel {

    @Id
    private String id;
    @NotNull
    private String idPoll;
    private String userName;
    private String valoracion;

    public FeedModel(){}

    public FeedModel(String id, String idPoll, String userName, String valoracion) {
        this.id = id;
        this.idPoll = idPoll;
        this.userName = userName;
        this.valoracion = valoracion;
    }

    public String getid() {
        return id;
    }    

    public void setid(String id) {
        this.id = id;
    }

    public String getidPoll() {
        return idPoll;
    }

    public void setidPoll(String idPoll) {
        this.idPoll = idPoll;
    }    

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getValoracion() {
        return valoracion;
    }

    public void setValoracion(String valoracion) {
        this.valoracion = valoracion;
    }
}

package com.feedback.adminback.service;

import com.feedback.adminback.model.PollModel;
import com.feedback.adminback.repository.PollRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PollService {

    @Autowired
    PollRepository pollRepository;

    // Read
    public List<PollModel> findAll() {
        return pollRepository.findAll();
    }

    // Read by Id
    public Optional<PollModel> findById(String id){
        return pollRepository.findById(id);
    }

    // Create
    public PollModel save(PollModel poll){
        return pollRepository.save(poll);
    }

    // Delete
    public boolean delete(PollModel poll){
        try{
            pollRepository.delete(poll);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    //Confirmar si existe
    public boolean existsByID(String id) {
        return(pollRepository.existsById(id));
    }

    public boolean deleteById(String id){
        if(pollRepository.existsById(id)) {
            pollRepository.deleteById(id);

            //además, hay que eliminar todos los feeds asociados
            


            return true;
        }
        else {
            return false;
        }
    }

}

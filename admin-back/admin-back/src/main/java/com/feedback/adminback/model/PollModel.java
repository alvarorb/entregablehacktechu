package com.feedback.adminback.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "polls")

public class PollModel {

    @Id
    private String id;
    @NotNull
    private String pollName;
    private String anon;

    public PollModel(){}

    public PollModel(String id, String pollName, String anon) {
        this.id = id;
        this.pollName = pollName;
        this.anon = anon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPollName() {
        return pollName;
    }

    public void setPollName(String pollName) {
        this.pollName = pollName;
    }


    public String getAnon() {
        return anon;
    }

    public void setAnon(String anon) {
        this.anon = anon;
    }
}

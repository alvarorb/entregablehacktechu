package com.feedback.adminback.controller;

import com.feedback.adminback.model.PollModel;
import com.feedback.adminback.service.PollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import com.feedback.adminback.service.FeedService;
import com.feedback.adminback.model.FeedModel;

@RestController
@CrossOrigin(origins ="*",methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE})
@RequestMapping("/feedback/v1")
public class PollController {

    @Autowired
    PollService PollService;

    @Autowired
    FeedService FeedService;

    // GET Polls
    @GetMapping("/polls")
    public List<PollModel> getPolls() {
        return PollService.findAll();
    }

    // GET Poll by ID
    @GetMapping("/polls/{id}")
    public Optional<PollModel> getPollId(@PathVariable String id){
        return PollService.findById(id);
    }

    // CREAR Poll
    @PostMapping("/polls")
    public PollModel postPosts(@RequestBody PollModel newPoll){
        PollService.save(newPoll);
        return newPoll;
    }

    // UPDATE Poll
    @PutMapping("/polls")
    public boolean putPolls(@RequestBody PollModel pollToUpdate){
        if (PollService.existsByID(pollToUpdate.getId())){
            PollService.save(pollToUpdate);
            return true;
        } else {
                return false;
        }
    }

    // BORRAR Poll
    @DeleteMapping("/polls")
    public boolean deletePolls(@RequestBody PollModel pollToDelete) {
        if (PollService.existsByID(pollToDelete.getId())){
            return PollService.delete(pollToDelete);
        } else {
            return false;
        }
    }

    // BORRAR Poll BY ID
    @DeleteMapping("/polls/{id}")
    public boolean deletePollsById(@PathVariable String id) {

        //1 obtenemos los feedbacks asociados a este poll y los eliminamos
        List<FeedModel> listaFeeds = FeedService.findByIdPoll(id);

        //los eliminamos
        String idFeed;
        boolean deleteById;
        for (int i = 0; i < listaFeeds.size(); ++i) {
            idFeed = listaFeeds.get(i).getid();
            deleteById = FeedService.deleteById(idFeed);
        }

        //2 borramos el poll
        return PollService.deleteById(id);
    }
}

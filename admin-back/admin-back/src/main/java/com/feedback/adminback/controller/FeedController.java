package com.feedback.adminback.controller;

import com.feedback.adminback.model.FeedModel;
import com.feedback.adminback.service.FeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin(origins ="*",methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE})
@RequestMapping("/feedback/v1")
public class FeedController {

    @Autowired
    FeedService FeedService;

    //GET feeds
    @GetMapping("/feeds")
    public List<FeedModel> getFeeds() {
        return FeedService.findAll();
    }

    //GET feeds BY ID Poll
    @GetMapping("/feeds/{idPoll}")
    public List<FeedModel> getFeedId(@PathVariable String idPoll){
        return FeedService.findByIdPoll(idPoll);
    }

    //CREATE new Feed
    @PostMapping("/feeds")
    public FeedModel postFeeds(@RequestBody FeedModel newFeed){
        FeedService.save(newFeed);
        return newFeed;
    }

    //UPDATE Feed
    @PutMapping("/feeds")
    public boolean putFeeds(@RequestBody FeedModel feedToUpdate){
        if (FeedService.existsByID(feedToUpdate.getidPoll())){
            FeedService.save(feedToUpdate);
            return true;
        } else {
                return false;
        }
    }

    @DeleteMapping("/feeds")
    public boolean deleteFeeds(@RequestBody FeedModel feedToDelete) {
        if (FeedService.existsByID(feedToDelete.getidPoll())){
            return FeedService.delete(feedToDelete);
        } else {
            return false;
        }
    }

    @DeleteMapping("/feeds/{id}")
    public boolean deleteFeedsById(@PathVariable String id) {
        return FeedService.deleteById(id);
    }

}
